package com.bank.product.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProductLimitUpdateRequest {
    @Schema(
            name = "productCode",
            title = "Product to update daily limit is",
            example = "INTERNAL_TRANSFER"
    )
    private String productCode;

    @Schema(
            name = "dailyLimit",
            title = "New daily limit value is",
            example = "150000"
    )
    private BigDecimal dailyLimit;
}
