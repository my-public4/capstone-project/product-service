package com.bank.product.controller;

import com.bank.product.service.ProductEventService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/api")
public class ProductEventController {

    @Autowired
    private ProductEventService productEventService;

    @GetMapping(value = "/v1/product-events", produces = "application/json")
    @Operation(summary = "Retrieve All Product-Events")
    public ResponseEntity<?> retrieveProductEvents() {
        return ResponseEntity.ok(productEventService.retrieveProductEvents());
    }
}
