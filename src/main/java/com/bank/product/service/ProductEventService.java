package com.bank.product.service;

import com.bank.product.entity.ProductEvent;
import com.bank.product.repository.ProductEventRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Slf4j
@Service
public class ProductEventService {
    @Autowired
    private ProductEventRepository productEventRepository;

    public List<ProductEvent> retrieveProductEvents() {
        return productEventRepository.findAll(
                Sort.by(Sort.Direction.DESC, "eventCreatedAt"));
    }

    @Transactional
    public void createProductLimitEvent(ProductEvent event) {
        productEventRepository.save(event);
    }
}
